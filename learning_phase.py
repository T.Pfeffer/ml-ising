import numpy as np
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import h5py

f = h5py.File("./train_IsingData.hdf5", "r")
train_IsingData_hdf5 = f["train_dataset"]
train_IsingData = np.array(train_IsingData_hdf5)
print(train_IsingData.shape)

f = h5py.File("./test_IsingData.hdf5", "r")
test_IsingData_hdf5 = f["test_dataset"]
test_IsingData = np.array(test_IsingData_hdf5)
print(test_IsingData.shape)

f = h5py.File("train_IsingLabels.hdf5", "r")
train_IsingLabels_hdf5 = f["train_labelset"]
train_IsingLabels = np.array(train_IsingLabels_hdf5)
print(train_IsingLabels.shape)

f = h5py.File("test_IsingLabels.hdf5", "r")
test_IsingLabels_hdf5 = f["test_labelset"]
test_IsingLabels = np.array(test_IsingLabels_hdf5)
print(test_IsingLabels.shape)

label_names = ['LT', 'HT']

L = train_IsingData.shape[1]
size_learning = train_IsingData.shape[0]
size_test = test_IsingData.shape[2]

plt.figure(figsize=(8,8))
for i in range(6*6):
  plt.subplot(6,6,i+1)
  plt.xticks([])
  plt.yticks([])
  plt.grid(False)
  rnd_sample = np.random.randint(0,size_learning)
  plt.imshow((train_IsingData[rnd_sample]+1)/2, cmap=plt.cm.binary)
  plt.xlabel(label_names[int(train_IsingLabels[rnd_sample])])
plt.show()

#train_IsingData = (train_IsingData+1)/2
#test_IsingData = (test_IsingData+1)/2

model = keras.Sequential([keras.layers.Flatten(input_shape=(L, L)),
                          keras.layers.Dense(100, activation=tf.nn.sigmoid, activity_regularizer=keras.regularizers.l2(0.01)),
                          keras.layers.Dense(2, activation=tf.nn.softmax, kernel_regularizer=keras.regularizers.l2(0.01) )
                          ])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(train_IsingData, train_IsingLabels, epochs=5)

test_loss, test_acc = model.evaluate(test_IsingData, test_IsingLabels)
print('Test accuracy:', test_acc)


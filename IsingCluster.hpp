#ifndef ISINGCLUSTER_HPP
#define ISINGCLUSTER_HPP


#include "pbc_system.hpp"
#include <fstream>
#include <random> 
#include <functional> 


class IsingCluster
{
  double T;
  int L;
  
  std::vector<int*> cluster;
  std::vector<int*> boundary;
  std::vector<int> nni;
  std::vector<int> nnj;

  std::mt19937 MyGenerator;
  std::uniform_real_distribution<> uni_dist;
  std::function<double()> rnd; 
  
  double MC_mag;
  double MC_mag2;
  int number_measurement_per_bin;
  int current_bin;
  std::vector<double> bins_magnetization;
  
public:
  
  pbc_system<int> sys;
  double max_number_measurements_magnetization;
  std::vector<double> time_series_magentization;
  double number_measurements_magnetization;
  
  // initial starting position
  
  int i,j;

  IsingCluster(int linear_size, double temp):
  T(temp),
  L(linear_size),
  sys(L,2),
  nni(4,0),
  nnj(4,0),
  MyGenerator(10),
  uni_dist(0,1),
  rnd(std::bind(uni_dist,MyGenerator)),
  MC_mag(0),
  MC_mag2(0),
  number_measurement_per_bin(500),
  current_bin(0),
  bins_magnetization(10000,0),
  max_number_measurements_magnetization(1e9),
  time_series_magentization(static_cast<int>(max_number_measurements_magnetization),0),
  number_measurements_magnetization(0)
  {}
  
  inline void constrcluster()
  {
    cluster.clear();
    
    nni={0,1,0,-1};
    nnj={1,0,-1,0};
    
    i=static_cast<int>((L-1)*rnd());
    j=static_cast<int>((L-1)*rnd());
    
    sys(i,j) = 2*sys(i,j); // include random spin into the cluster
    boundary.push_back(&sys(i,j));
    cluster.push_back(&sys(i,j));
  }
  
  inline void growcluster(const int& l, const int& m)
  {
    if(boundary.empty()==0)
    {
      boundary.pop_back();

      int number_nearest_neigbors = 4;
      for(int k=0; k<number_nearest_neigbors; k++)
      {

        if( sign( sys( sys.pbc(l,nni[k]), sys.pbc(m,nnj[k]) ) ) == sign( sys(l,m) ) && rnd() <= (1.0-exp(-2.0/T)) )
        {
          
          int x = sys.pbc(l, nni[k]);
          int y = sys.pbc(m, nnj[k]);
          
          if( sys(x,y)*sys(x,y) == 1 ) // not included yet
          {
            sys(x,y) = 2 * sys(x,y);
            boundary.push_back( &sys( sys.pbc(l,nni[k]), sys.pbc(m,nnj[k]) ) );
            cluster.push_back( &sys(x,y) );
          }
          growcluster( sys.pbc(l,nni[k]), sys.pbc(m,nnj[k]) );
          
        }
      }
    }
  }
  
  inline void flipcluster()
  {
    for(int k=0; k<cluster.size(); k++)
      *cluster[k] = -1*sign((*cluster[k]));
  }
  
  inline int sign(int x)
  {
    if(x>=0) return 1;
    else return -1;
  }
  
  inline double magnetization()
  {
    return sys.mag();
  }

  inline void estimator()
  {
    double current_mag = magnetization();
    time_series_magentization[number_measurements_magnetization] = current_mag;
    MC_mag += std::fabs(current_mag);
    MC_mag2 += current_mag * current_mag;
    number_measurements_magnetization++;
    if(number_measurements_magnetization == number_measurement_per_bin)
    {
      bins_magnetization[current_bin] += MC_mag/number_measurements_magnetization;
      MC_mag = 0;
      current_bin ++;
      number_measurements_magnetization = 0;
    }
  }
  
  inline void print_magnetization()
  {
    double average_m = 0;
    double average_m2 = 0;
    for(int i=0; i<current_bin; i++)
    {
      average_m += bins_magnetization[i]/current_bin;
      average_m2 += bins_magnetization[i]*bins_magnetization[i]/current_bin;
    }
    std::cout << average_m << " +/- " << std::sqrt(average_m2 - average_m * average_m)/std::sqrt(current_bin) << std::endl;
  }
  
  inline void output_snapshot(std::string filename)
  {
    std::ofstream out(filename);
    for(int i=0; i<L; i++)
      for(int j=0; j<L; j++)
        out << sys(i,j) << std::endl;
    out.close();
  }
  
  inline void output_time_series()
  {
    std::ofstream out("./time_series_mag");
    
    for(auto iter : time_series_magentization)
      out << iter << std::endl;
    
    out.close();
    
  }
  
};



#endif

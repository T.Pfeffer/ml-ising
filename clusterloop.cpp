#include <iostream>
#include "IsingCluster.hpp"
#include <cmath>
using namespace std;


int main(int argc, char *argv[])
{
  int system_size = atof(argv[1]);
  double temperature = atof(argv[2]);
  int number_of_snapshots = atof(argv[3]);
  
  int current_snapshot = 0;
  IsingCluster m(system_size, temperature);

  for(long long int k=1; k<=pow(2,20); k++)
  {
    m.constrcluster();
    m.growcluster(m.i,m.j);
    m.flipcluster();
    if(k>10000)
    {
       if(k%2 == 0)
         m.estimator();
       if(k%1000 == 0)
       {
         m.output_snapshot("snapshots/"+std::to_string(current_snapshot));
         current_snapshot ++;
         if( current_snapshot == number_of_snapshots )
           break;
       }
    }

    if( k%100000 == 0)
    {
      std::cout << k << ": m = ";
      m.print_magnetization();
    }
  }
  std::cout << "T = " << temperature << ": m = "; m.print_magnetization();
 return 0; 
}

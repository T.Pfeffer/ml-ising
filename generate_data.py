import os, sys
import numpy as np
import subprocess
import h5py


os.system('g++ -std=c++0x -O3 -o runcluster clusterloop.cpp')

system_size = 16
number_of_snapshots = 1000;
number_train_snapshots = int(number_of_snapshots * 0.9)  # use 90% of snapshots for training
number_test_snapshots = int(number_of_snapshots * 0.1)   # the rest for testing

low_temperature_list = [1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2.0, 2.05, 2.1, 2.15, 2.2]
high_temperature_list = [2.3, 2.35, 2.4, 2.45, 2.5, 2.55, 2.6, 2.65, 2.7, 2.75, 2.8, 2.85, 2.9, 2.95]
temperature_list = low_temperature_list + high_temperature_list

number_of_temperature_points = len(temperature_list)

train = h5py.File('train_IsingData.hdf5', 'w')
Data_train = train.create_dataset( "train_dataset", (number_of_temperature_points * number_train_snapshots, system_size, system_size) )
test = h5py.File('test_IsingData.hdf5', 'w')
Data_test = test.create_dataset( "test_dataset", (number_of_temperature_points * number_test_snapshots, system_size, system_size) )

if not os.path.exists('./snapshots'):
  os.makedirs('./snapshots')

for temperature_point in range( len(temperature_list) ):
  command_line_arguments = str(system_size) + ' ' + str(temperature_list[temperature_point]) + ' ' + str(number_of_snapshots)
  p = subprocess.call('./runcluster ' + command_line_arguments, shell=True)

  for i in range(number_of_snapshots):
    if i < int(number_of_snapshots * 0.9):
      index = temperature_point*number_train_snapshots + i
      Data_train[index] = np.reshape( np.loadtxt('./snapshots/'+str(i)), (system_size, system_size) )
    else:
      index = temperature_point*number_test_snapshots + i-number_train_snapshots
      Data_test[index] = np.reshape( np.loadtxt('./snapshots/'+str(i)), (system_size, system_size) )

train_labels = h5py.File('train_IsingLabels.hdf5', 'w')
number_low_temperature_snapshots = len(low_temperature_list) * number_train_snapshots
number_high_temperature_snapshots = len(high_temperature_list) * number_train_snapshots
train_labels.create_dataset( "train_labelset", data = np.append( np.zeros(number_low_temperature_snapshots), np.ones(number_high_temperature_snapshots) ) )
test_labels = h5py.File('test_IsingLabels.hdf5', 'w')
number_low_temperature_snapshots = len(low_temperature_list) * number_test_snapshots
number_high_temperature_snapshots = len(high_temperature_list) * number_test_snapshots
test_labels.create_dataset( "test_labelset", data = np.append( np.zeros(number_low_temperature_snapshots), np.ones(number_high_temperature_snapshots) ) )


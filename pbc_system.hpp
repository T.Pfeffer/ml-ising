#ifndef PBC_SYSTEM_HPP
#define PBC_SYSTEM_HPP


#include <iostream>
#include <vector>
#include <random>
#include <functional>
#include <cstdlib>

template<class T>
class pbc_system
{

  int linear_size; // size of square system
  std::vector<std::vector<T> > system;
  
public:

  pbc_system();

  pbc_system(int size, int spin):
  linear_size(size),
  system(std::vector<std::vector<T>>(size,std::vector<T>(size,0)))
  {
	  linear_size=size;
	  for(int i=0; i<linear_size; i++)
		  for(int j=0; j<linear_size; j++)
          system[i][j]=((rand()%spin)-0.5)*2;
  };


  pbc_system(int size):
  linear_size(size),
  system(std::vector<T>(size,std::vector<T>(size,0)))
  {};

//  inline std::ostream& operator<<(std::ostream& os, pbc_system<T> x)
//  {
//    os<< "[";
//    for(int i=0; i<linear_size; i++)
//    {
//      os << "(" ;
//      for(int j=0; j<linear_size; j++)
//      {
//          os <<  x(i,j);
//          if(j <= linear_size-2)
//            os << ",";
//      }
//       os << ")";
//       if( i<=linear_size-2 )
//         os << ",";
//    }
//    os << "]";
//    return os;
//  }

  inline const T operator()(int i, int j) const
  {
    return system[i][j];
  }
  
  inline T& operator()(int i, int j)
  {
    return system[i][j];
  }
  
  inline double mag()
  {
    double sum=0;
    for(int i=0; i<linear_size; i++)
      for(int j=0; j<linear_size; j++)
        sum += system[i][j];
    sum /= (linear_size*linear_size);
    return sum;
  }
  
  inline int pbc(int position, int step)
  {
    if ((position+step)<=(linear_size-1) && (position+step)>=0)
      return position+step;
    else if ((position+step)>(linear_size-1) && (position+step) != (2*(linear_size-1)))
      return (position+step)%(linear_size-1)-1;
    else if ((position+step)<0)
      return position+step+linear_size;
    else if((position+step)==(2*(linear_size-1)))
      return position;
    else
    {
      std::cout << "Error in calculating periodic boundary conditions" << std::endl;
      exit(1);
    }
  }
  
  inline double energy()
  {
    double energy=0;
    for(int i=0; i<linear_size; i++)
      for(int j=0; j<linear_size; j++)
      {
        double nearest_neighbors = static_cast<double>(system[pbc(i,-1)][j]+system[pbc(i,1)][j]+system[i][pbc(j,-1)]+system[i][pbc(j,1)]);
        energy += system[i][j] * nearest_neighbors;
      }
    return energy/2.0; //because of double counting
  }
   
};

#endif
